#!/usr/bin/env bash

# Install Chrome if not already installed 
if [ ! -e /usr/bin/google-chrome-stable ]
then
   echo "Installing Google chrome"
   sudo apt-get install google-chrome-stable
   sudo wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
   sudo dpkg -i google-chrome-stable_current_amd64.deb
   sudo sh -c 'echo "deb https://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list'
fi

# Install node modules
npm install --no-audit
