//require('env2')('.env');
const seleniumServer = require("selenium-server");
const chromedriver = require("chromedriver");
require('babel-core/register');
const PKG = require('./package.json');
var SCREENSHOT_PATH = "./screenshots/";
var APP_URL = "http://ec2-18-207-205-54.compute-1.amazonaws.com/";
var APP_URL_LOCAL = "http://localhost:3000/";

const config = {
  "src_folders": [
    "tests/e2e"
  ],

  "output_folder": "./reports",

  "page_objects_path": './tests/pages',

  "selenium": {
    "start_process": true,
    "server_path": seleniumServer.path,
    "log_path": "",
    "host": "127.0.0.1",
    "port": 4444,
    "cli_args": {
      "webdriver.chrome.driver" : chromedriver.path
    }
  },

  "live_output" : true,

  "test_workers" : {
       "enabled" : false,
       "workers" : "auto"
  },

"test_settings": {
    "default": {
      "selenium_port": 4444,
      "selenium_host": "127.0.0.1",
      "use_xpath": true,
      "silent": true,
      "skip_testcases_on_fail": false,
      "screenshots": {
                       "enabled": true,
                       "on_failure":true,
                       "on_error":false,
                       "path": SCREENSHOT_PATH
                    }
      },

      "local": {
            "launch_url": APP_URL_LOCAL
      },

      "staging": {
            "launch_url": APP_URL,
             "desiredCapabilities": {
                             "browserName": "chrome",
                             "javascriptEnabled": true,
                             "acceptSslCerts": true,
                             "chromeOptions": {
                                "args": [ 'headless', 'no-sandbox'],
                                "binary": "/usr/bin/google-chrome-stable"
                             }
                  }
      },

      "globals": {
               "waitForConditionTimeout": 15000 // on localhost sometimes internet is slow so wait...
      }
    },

     "chrome": {
          "desiredCapabilities": {
            "browserName": "chrome",
            "javascriptEnabled": true,
            "acceptSslCerts": true,
            "chromeOptions": {
               "args": [ 'headless', 'no-sandbox']
            }
          }
        },

        "firefox" : {
          "desiredCapabilities": {
            "browserName": "firefox",
            "marionette": false,
            "version": "33"
          }
      }

    }

    function padLeft (count) { // theregister.co.uk/2016/03/23/npm_left_pad_chaos/
      return count < 10 ? '0' + count : count.toString();
    }

    var FILECOUNT = 0; // "global" screenshot file count

    function imgpath (browser) {
      var a = browser.options.desiredCapabilities;
      var meta   = [a.platform];
      meta.push(a.browserName ? a.browserName : 'any');
      meta.push(a.version ? a.version : 'any');
      meta.push(a.name); // this is the test filename so always exists.
      var metadata = meta.join('-').toLowerCase().replace(/ /g, '');
      return SCREENSHOT_PATH + metadata + '_' + padLeft(FILECOUNT++) + '_';
    }

    module.exports = config;
    module.exports.imgpath = imgpath;
    module.exports.SCREENSHOT_PATH = SCREENSHOT_PATH;
