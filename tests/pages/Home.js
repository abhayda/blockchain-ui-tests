
const homePageCommands = {
    url() {
        console.log('## URL Path ' + this.api.launchUrl);
        return this.api.launchUrl;
    },
    clickSubmitButton() {
        return this.click('@submitButton');
    },
    enterOfferId(offerId) {
        return this.setValue('@offerIdTextbox', offerId);
    },
    enterOfferName(offerName) {
        return this.setValue('@offerNameTextbox', offerName);
    },
    enterOfferProductUPC(offerUPC) {
        return this.setValue('@offerProductUPCTextbox', offerUPC);
    },
    enterOfferDiscount(offerDiscount) {
        return this.setValue('@offerDiscountTextbox', offerDiscount);
    },
    enterOfferCouponLimit(offerCouponLimit) {
        return this.setValue('@offerCouponLimitTextbox', offerCouponLimit);
    },
    enterOfferDescription(offerDesc) {
        return this.setValue('@offerCouponDescription', offerDesc);
    }
};

export default {

    commands: [homePageCommands],
    elements: {
        offerIdTextbox: {
            selector: "//input[@id='offerId']",
            locateStrategy: 'xpath'
        },
        offerNameTextbox: {
            selector: "//input[@id='offerName']",
            locateStrategy: 'xpath'
        },
        offerProductUPCTextbox: {
            selector: "//input[@id='offerUPC']",
            locateStrategy: 'xpath'
        },
        offerDiscountTextbox: {
            selector: "//input[@id='offerDiscount']",
            locateStrategy: 'xpath'
        },
        submitButton: {
            selector: "//button[@type='submit']",
            locateStrategy: 'xpath'
        },
        successMessageBox: {
            selector: "//*[@id=\"createOfferSection\"]/div/div",
            locateStrategy: 'xpath'
        },
        blockHistorySection: {
            selector: "//*[@id='blockHistorySection']/header/div[2]",
            locateStrategy: 'xpath'
        },
        blocksContainer: {
            selector: "//*[@id=\"blockHistorySection\"]/ul",
            locateStrategy: 'xpath'
        },
        transactionHistorySection: {
            selector: "//*[@id=\"transactionHistorySection\"]/header",
            locateStrategy: 'xpath'
        },
        transactionsContainer: {
            selector: "//*[@id=\"transactionHistorySection\"]/ul",
            locateStrategy: 'xpath'
        },
        latestBlockNumber: {
            selector: "//*[@id='blockHistorySection']/ul/li[1]/a/div/div[1]/span[2]",
            locateStrategy: 'xpath'
        },
        latestTransactionNumber: {
            selector: "//*[@id=\"blockHistorySection\"]/ul/li[1]/a/div/div[1]/span[2]",
            locateStrategy: 'xpath'
        },
        offerCouponLimitTextbox: {
            selector: "//input[@id='offerCouponLimit']",
            locateStrategy: 'xpath'
        },
        offerCouponDescription: {
            selector: "//input[@id='offerDescription']",
            locateStrategy: 'xpath'
        }
    }
};