require('../../nightwatch.conf.js');
var randomNumber = Math.floor(Math.random() * 10000);
var offerId = "Pepsico-" + randomNumber;

let homePage;

export default {
    before(client) {
        console.log('## Launching Application ##');
        homePage = client.page.Home();
        client.maximizeWindow();
    },

    after(client) {
        console.log('## Tests complete ##');
        client.end();
    },

    'Check New Transaction is created': async (client) => {
        console.log('## Starting Test: New Transaction ##');

        let actualBlockNumber;

        homePage
            .navigate()
            .waitForElementVisible("@submitButton", 5000)
            .waitForElementVisible('@transactionHistorySection', 1000)
            .waitForElementVisible('@transactionsContainer', 1000)
            .getText('@latestBlockNumber', (result) => actualBlockNumber = +result.value)
            .getText('@latestTransactionNumber', () =>
                homePage.expect.element('@latestTransactionNumber').to.contain.text(actualBlockNumber)
            );
    }
};