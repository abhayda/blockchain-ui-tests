require('../../nightwatch.conf.js');
var randomNumber = Math.floor(Math.random() * 10000);
var offerId = "Pepsico-" + randomNumber;

let homePage;

export default {
    before(client) {
        console.log('## Launching Application ##');
        homePage = client.page.Home();
        client.maximizeWindow();
    },

    after(client) {
        console.log('## Tests complete ##');
        client.end();
    },

    'Check New Block Mined': async (client) => {
        console.log('## Starting Test: New block mine ##');

        let actualBlockNumber;

        homePage
            .navigate()
            .waitForElementVisible("@submitButton", 5000)
            .waitForElementVisible('@blockHistorySection', 1000)
            .waitForElementVisible('@blocksContainer', 1000)
            .getText('@latestBlockNumber', (result) => actualBlockNumber = +result.value)
            .enterOfferId(offerId)
            .enterOfferName("Pepsi 99 offer")
            .enterOfferProductUPC("123456789011")
            .enterOfferDiscount("00.99")
            .enterOfferCouponLimit("10")
            .clickSubmitButton()
            .waitForElementVisible("@successMessageBox", 5000)
            .getText('@latestBlockNumber', () =>
                homePage.expect.element('@latestBlockNumber').to.contain.text(actualBlockNumber + 1)
            );
    },
};