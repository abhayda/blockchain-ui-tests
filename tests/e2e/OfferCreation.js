var config = require('../../nightwatch.conf.js');
var randomNumber = Math.floor(Math.random() * 10000);
var offerId = "Pepsico-" + randomNumber;

let homePage;

export default {
    before(client) {
        console.log('## Launching Application ##');
        homePage = client.page.Home();
        client.maximizeWindow();
    },

    after(client) {
        console.log('## Tests complete ##');
        client.end();
    },

    'Create Offer': (client) => {
        console.log('## Starting Test: Create Offer ##');
        homePage.navigate();
        client.pause(5000);
        homePage.expect.element('@submitButton').to.be.visible;
        homePage.enterOfferId(offerId);
        homePage.enterOfferName("Pepsi 99 offer");
        homePage.enterOfferProductUPC("123456789011");
        homePage.enterOfferDiscount("00.99");
        homePage.enterOfferCouponLimit("10");
        homePage.enterOfferDescription("This offer is on all products of Pepsi, limited only in US and Canada.");
        homePage.clickSubmitButton();
        client.pause(50000);
        homePage.expect.element('@successMessageBox').to.contain.text('Offer created successfully');
        client.saveScreenshot(config.imgpath(client) + 'offerId-creation.png');
    },

    'Offer Already Exists': (client) => {
        console.log('## Starting Test: Create Offer ##');
        homePage.navigate();
        client.pause(5000);
        homePage.expect.element('@submitButton').to.be.visible;
        homePage.enterOfferId(offerId);
        homePage.enterOfferName("Pepsi 99 offer");
        homePage.enterOfferProductUPC("123456789011");
        homePage.enterOfferDiscount("00.99");
        homePage.enterOfferCouponLimit("10");
        homePage.clickSubmitButton();
        client.pause(50000);
        homePage.expect.element('@successMessageBox').to.contain.text('This offer already exists');
        client.saveScreenshot(config.imgpath(client) + 'offer-already-exists.png');
    }
};